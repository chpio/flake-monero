{
    description = "monero";

    inputs = {
        nixpkgs.url = github:NixOS/nixpkgs;
        flake-utils.url = "github:numtide/flake-utils";
    };

    outputs = { self, flake-utils, nixpkgs }:
        flake-utils.lib.eachSystem ["x86_64-linux"] (system:
            let
                pkgs = import nixpkgs { inherit system; };

                arch = if pkgs.stdenv.isx86_64 then "x86-64"
                    else if pkgs.stdenv.isi686 then "i686"
                    else if pkgs.stdenv.isAarch64 then "armv8-a"
                    else throw "unsupported architecture";

                monero = with pkgs; with lib; stdenv.mkDerivation rec {
                    pname = "monero";
                    version = "0.17.2.3";

                    src = fetchFromGitHub {
                        owner = "monero-project";
                        repo = "monero";
                        rev = "v${version}";
                        sha256 = "BtcunPtIqB3toIH+Z8IsQ3POs/NZQJhfKNBB7UJKXVk=";
                        fetchSubmodules = true;
                    };

                    patches = [
                        ./monero-use-system-libraries.patch
                    ];

                    postPatch = ''
                        # remove vendored libraries
                        rm -r external/{miniupnp,randomx,rapidjson,unbound}
                        # export patched source for monero-gui
                        cp -r . $source
                    '';

                    nativeBuildInputs = [ cmake pkgconfig ];

                    buildInputs = [
                        boost miniupnpc openssl unbound
                        zeromq pcsclite readline
                        libsodium hidapi randomx rapidjson
                        protobuf
                    ];

                    cmakeFlags = [
                        "-DCMAKE_BUILD_TYPE=Release"
                        "-DUSE_DEVICE_TREZOR=ON"
                        "-DBUILD_GUI_DEPS=ON"
                        "-DReadline_ROOT_DIR=${readline.dev}"
                        "-DRandomX_ROOT_DIR=${randomx}"
                    ] ++ optional stdenv.isDarwin "-DBoost_USE_MULTITHREADED=OFF";

                    outputs = [ "out" "source" ];

                    meta = with lib; {
                        description = "Private, secure, untraceable currency";
                        homepage    = "https://getmonero.org/";
                        license     = licenses.bsd3;
                        platforms   = platforms.all;
                        maintainers = with maintainers; [ ehmry rnhmjoj ];
                    };
                };
            
                monero-gui = with pkgs; with lib; stdenv.mkDerivation rec {
                    pname = "monero-gui";
                    version = "0.17.2.3";

                    src = fetchFromGitHub {
                        owner  = "monero-project";
                        repo   = "monero-gui";
                        rev    = "v${version}";
                        sha256 = "T3bU5iZ654I9SQlvqv2HnGFR+WeT17SbFKsB7rEvHrU=";
                    };

                    nativeBuildInputs = with pkgs.qt5; [
                        cmake pkgconfig wrapQtAppsHook
                        (getDev qttools)
                    ];

                    buildInputs = with pkgs.qt5; [
                        qtbase qtdeclarative qtgraphicaleffects
                        qtmultimedia qtquickcontrols qtquickcontrols2
                        qtxmlpatterns
                        monero miniupnpc unbound readline
                        randomx libgcrypt libgpgerror
                        boost libunwind libsodium pcsclite
                        zeromq hidapi rapidjson quirc
                    ];

                    postUnpack = ''
                        # copy monero sources here
                        # (needs to be writable)
                        cp -r ${monero.source}/* source/monero
                        chmod -R +w source/monero
                    '';

                    patches = [ ./monero-gui-move-log-file.patch ];

                    postPatch = ''
                        # set monero-gui version
                        substituteInPlace src/version.js.in \
                        --replace '@VERSION_TAG_GUI@' '${version}'
                        # use monerod from the monero package
                        substituteInPlace src/daemon/DaemonManager.cpp \
                        --replace 'QApplication::applicationDirPath() + "' '"${monero}/bin'
                        # only build external deps, *not* the full monero
                        substituteInPlace CMakeLists.txt \
                        --replace 'add_subdirectory(monero)' \
                                    'add_subdirectory(monero EXCLUDE_FROM_ALL)'

                        # use nixpkgs quirc
                        substituteInPlace CMakeLists.txt \
                        --replace 'add_subdirectory(external)' ""
                    '';

                    cmakeFlags = [ "-DARCH=${arch}" ];

                    desktopItem = makeDesktopItem {
                        name = "monero-wallet-gui";
                        exec = "monero-wallet-gui";
                        icon = "monero";
                        desktopName = "Monero";
                        genericName = "Wallet";
                        categories  = "Network;Utility;";
                    };

                    postInstall = ''
                        # install desktop entry
                        install -Dm644 -t $out/share/applications \
                        ${desktopItem}/share/applications/*
                        # install icons
                        for n in 16 24 32 48 64 96 128 256; do
                        size=$n"x"$n
                        install -Dm644 \
                            -t $out/share/icons/hicolor/$size/apps/monero.png \
                            $src/images/appicons/$size.png
                        done;
                    '';

                    meta = {
                        description  = "Private, secure, untraceable currency";
                        homepage     = "https://getmonero.org/";
                        license      = licenses.bsd3;
                        platforms    = platforms.all;
                        badPlatforms = platforms.darwin;
                        maintainers  = with maintainers; [ rnhmjoj ];
                    };
                };
            in {
                packages.monero = monero;
                packages.monero-gui = monero-gui;
            }
        );
}
